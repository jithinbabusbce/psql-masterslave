## Test Using docker-compose
```
docker-compose up -d
```

## Create table in master database
```
docker exec -it psql-masterslave_postgres_1 bash
su postgres
psql -h localhost -U postgres
\c test
CREATE TABLE replica_test (hakase varchar(100));
INSERT INTO replica_test VALUES ('test');
```

## Login to slave container to see the replication
```
docker exec -it psql-masterslave_pg_slave_1 bash
psql -h localhost -U postgres
\c test
select * from replica_test;
```

# Pushing image to apple repo
## Build and push master image
```
cd master/
docker build -t psql-masterslave_postgres .
cd ..
Tag and push your image
```

## build and push slave image
```
cd slave/
docker build -t psql-slave .
cd ..
Tag and push your image
```

### Kuberenetes YAML files
cd kuberenetes
```
> deployment-master.yaml	  -> Compare with your orginal file and append environment variables, image name
> service-master.yaml    -> Make sure to give service name as postgres

  metadata:
    name: postgres

> deployment-slave.yaml   -> Compare with your orginal file and append environment variables, image name
```
